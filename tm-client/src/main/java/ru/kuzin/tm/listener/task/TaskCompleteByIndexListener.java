package ru.kuzin.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.request.TaskCompleteByIndexRequest;
import ru.kuzin.tm.event.ConsoleEvent;
import ru.kuzin.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-complete-by-index";

    @NotNull
    private static final String DESCRIPTION = "Complete task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskCompleteByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken());
        request.setIndex(index);
        taskEndpoint.completeTaskByIndex(request);
    }

}