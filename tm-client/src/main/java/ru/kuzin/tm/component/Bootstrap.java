package ru.kuzin.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.api.endpoint.*;
import ru.kuzin.tm.api.service.ICommandService;
import ru.kuzin.tm.api.service.ILoggerService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.api.service.ITokenService;
import ru.kuzin.tm.event.ConsoleEvent;
import ru.kuzin.tm.exception.system.ArgumentNotSupportedException;
import ru.kuzin.tm.exception.system.CommandNotSupportedException;
import ru.kuzin.tm.listener.AbstractListener;
import ru.kuzin.tm.util.SystemUtil;
import ru.kuzin.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@NoArgsConstructor
@Component
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.kuzin.tm.command";

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void createConsoleEvent(@Nullable final String command) {
        @Nullable final AbstractListener listener = Arrays.stream(listeners)
                .filter(m -> command.equals(m.getName()))
                .findFirst()
                .orElse(null);
        if (listener == null) throw new CommandNotSupportedException(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void processArguments(@Nullable final String argument) throws Exception {
        @Nullable final AbstractListener listener = Arrays.stream(listeners)
                .filter(m -> argument.equals(m.getArgument()))
                .findFirst()
                .orElse(null);
        if (listener == null && argument != null) throw new ArgumentNotSupportedException(argument);
        if (listener == null) throw new ArgumentNotSupportedException();
        publisher.publishEvent(listener.getName());
    }

    private boolean processArguments(@Nullable final String[] arguments) throws Exception {
        if (arguments == null || arguments.length < 1) return false;
        @Nullable final String arg = arguments[0];
        processArguments(arg);
        return true;
    }

    private void processCommands() {
        try {
            System.out.println("ENTER COMMAND: ");
            @NotNull final String command = TerminalUtil.nextLine();
            createConsoleEvent(command);
            System.out.println("[OK]");
            loggerService.command("Executing command: " + command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.out.println("[FAIL]");
        }
    }

    public void run(@Nullable final String[] args) throws Exception {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) processCommands();
    }

}