package ru.kuzin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kuzin.tm.component.Bootstrap;
import ru.kuzin.tm.configuration.ClientConfiguration;

public final class Application {

    public static void main(@Nullable final String... args) throws Exception {
        @NotNull ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}