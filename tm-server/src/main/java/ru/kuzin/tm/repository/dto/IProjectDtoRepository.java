package ru.kuzin.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.kuzin.tm.dto.model.ProjectDTO;

@Repository
public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {
}