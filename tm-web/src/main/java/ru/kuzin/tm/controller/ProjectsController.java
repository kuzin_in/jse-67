package ru.kuzin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.kuzin.tm.api.service.IProjectService;

@Controller
public class ProjectsController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectService.findAll());
    }

}