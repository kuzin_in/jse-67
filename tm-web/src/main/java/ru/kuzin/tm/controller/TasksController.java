package ru.kuzin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.kuzin.tm.api.service.IProjectService;
import ru.kuzin.tm.api.service.ITaskService;

@Controller
public class TasksController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping("/tasks")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}